 $(document).ready(function () {
    var height = window.innerHeight;
    height_fin = height + "px";
    $('#content').css('min-height', height_fin);
    var height2 = height - 100;
    height_fin2 = height2 + "px";
    $('#tabla_menu').css('min-height', height_fin2);
    
    setInterval("Tamanio()", 500);
});
 
 //Return xmlDoc object
function getXmlDoc (data) {

    try { 
        
        //Internet Explorer
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(data);

    } catch (e) {
        try { 

            // Firefox, Mozilla, Opera, etc.
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(data, "text/xml");
        }
        catch (e) {
            console.log(e.message);
            return;
        }
    }
    return xmlDoc;
}

 //Consulta la fecha de ingreso de los insumos para presentarla en el home de la app
        function cargarUltimaFecha() {

            var isConsultada = false;
            var tipoProducto = "";

            var bhRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
                "<soapenv:Body>" +
                "<tem:ObtenerInsumosPorMunicipioTipoProducto>" +
                "<tem:idMunicipio>" + 1 + "</tem:idMunicipio>" +
                "<tem:idDepartamento>" + 17 + "</tem:idDepartamento>" +
                "<tem:idTipoProducto>"+ tipoProducto +"</tem:idTipoProducto>" +
                "</tem:ObtenerInsumosPorMunicipioTipoProducto>" +
                "</soapenv:Body>" +
                "</soapenv:Envelope>";
                $.ajax({
                    type: "POST",
                    url: "http://190.60.251.18/Insumos/ServicioInsumos.svc",
                    data: bhRequest,
                    contentType: "text/xml",
                    dataType: "text",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("SOAPAction", "http://tempuri.org/IServicioInsumos/ObtenerInsumosPorMunicipioTipoProducto");
                    },
                    success: function (data) {

                        xmlDoc = getXmlDoc(data);
                        var fecha;
                        var nodes = xmlDoc.getElementsByTagName("InsumoDTO");
                        count = nodes.length;

                        if (count > 0) {

                            var text = "";
                            
                            fecha = xmlDoc.getElementsByTagName("FechaUltimaActualizacion")[0].childNodes[0];
                            if(typeof fecha !== "undefined") {
                                fecha = xmlDoc.getElementsByTagName("FechaUltimaActualizacion")[0].childNodes[0].nodeValue;
                                $("#lblUltimaFecha").text("Los precios corresponden al mes de " + formatDate(fecha));
                            }else {
                                $("#lblUltimaFecha").text("");
                            }
                            
                            $(data).find("ObtenerInsumosPorMunicipioTipoProductoResponse").each(function () {
                            });
                            
                        } else {
                            console.log("no se ha encontrado fecha de actualizacion");
                        }
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                    }
                });
        }

function formatDate (value) {

    var date = value.split("-");
    var newDate = formatMonth(date[1]) + " de " + date[0];
    return newDate;
}

function formatMonth (month) {
    var month = month.replace('0','');
    var monthNames = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ];

    return monthNames[month - 1];

}

//Adaptar el tamaño de la aplicación a cualquier pantalla
function Tamanio() {
    var height = window.innerHeight - $('.footer').height();
    height_fin = height + "px";
    $('#content').css('min-height', height_fin);
    var height2 = height - 120;
    height_fin2 = height2 + "px";
    $('#tabla_menu').css('min-height', height_fin2);
}